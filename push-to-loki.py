#!/usr/bin/env python3
# Run a command and push the output to grafana/loki api
import requests
import json
import datetime
import pytz
import platform
import subprocess
import sys
import os
from access_token import AUTH

def push_message_to_loki(return_code, msg, job_name, command):
    host = platform.node()
    curr_datetime = datetime.datetime.now(pytz.timezone('Australia/Perth'))
    #curr_datetime = curr_datetime.isoformat('T')
    curr_datetime = curr_datetime.timestamp() * 1000000000

    # push msg log into grafana-loki
    url = 'https://logs-prod-004.grafana.net/loki/api/v1/push'
    headers = {
        'Content-type': 'application/json',
        #'Authorization': 'Bearer {}'.format(access_token)
    }

    labels = {
        'source': "push-to-loki",
        'job': job_name,
        'host': host,
        'exit': return_code,
    }
    payload = {
        'streams': [
            {
                'stream': labels,
                'values': [
                    [str(int(curr_datetime)), f"{host} {return_code} - {job_name}\n{command}\n{msg}"]                
                ]
            }
        ]
    }
    payload = json.dumps(payload)
    #print(payload)
    response = requests.post(url, data=payload, headers=headers, auth=AUTH)

    #import pprint
    #pprint.pprint(response.text)
    print(response.text)
    #pprint.pprint(response.raw)
    # end pushing


def run_command_capture_output(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    complete_stdout = ''
    while True:
        stdout = process.stdout.readline()
        stderr = process.stderr.readline()
        #print((stdout, stderr, process.poll()))
        if stdout == b'' and stderr == b'' and process.poll() is not None:
            break
        if stdout:
            print(stdout.strip().decode('UTF-8'))
            complete_stdout += stdout.decode('UTF-8')
        if stderr:
            print(f"[ERR] {stderr.strip().decode('UTF-8')}")
            complete_stdout += f"[ERR] {stderr.decode('UTF-8')}"
    rc = process.poll()
    return (rc, complete_stdout)



if __name__ == "__main__":
    command = sys.argv[1:]
    job_name = os.getenv('JOB_NAME', 'push-to-loki-default-job')
    print(command)
    (return_code, output) = run_command_capture_output(command)
    push_message_to_loki(return_code, output, job_name, command)